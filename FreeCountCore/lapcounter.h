#ifndef LAPCOUNTER_H
#define LAPCOUNTER_H

#include <QtCore>

class LapCounter : public QObject
{
    Q_OBJECT
public:
    virtual ~LapCounter() {}

    virtual void startRace() = 0;        // Only for lapZ counters, do nothing on robitronic
    virtual void stopRace() = 0;         // Only for lapZ counters, do nothing on robitronic

    virtual void writeCarId(quint8 carId) = 0; // Only for lapZ counters, do nothing on robitronic

    virtual bool isCarIdWritable() = 0;  // Only true for lapZ counters, false on robitronic

signals:
    void carDetected(quint32 timestamp, quint16 carId, quint8 hitNumber, quint8 signalStrength);
    void heartBeat(quint32 timestamp);

};

#endif // LAPCOUNTER_H
