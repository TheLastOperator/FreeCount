#include "freecountcore.h"


FreeCountCore::FreeCountCore()
{
    // Init database

    m_database = QSqlDatabase::addDatabase("QSQLITE");
    m_database.setHostName("localhost");
    m_database.setDatabaseName("freecount.db");
    if (!m_database.open()) {
        qDebug() << "Failed to open database";
    }

    m_model = new QSqlQueryModel;

    QString str = "SELECT "
                  "    firstname, "
                  "    lastname, "
                  "    nickname, "
                  "    ( "
                  "        SELECT count(*) FROM Lap WHERE Lap.pilot = Pilot.id "
                  "    ) - 1 "
                  "    AS laps, "
                  "    ( "
                  "        SELECT t1.timestamp - ( "
                  "            SELECT timestamp "
                  "            FROM Lap t2 "
                  "            WHERE "
                  "                t2.id < t1.id "
                  "                AND t2.pilot = t1.pilot "
                  "                AND t2.run = t1.run "
                  "            ORDER BY t2.id DESC LIMIT 1 "
                  "        ) "
                  "        AS time"
                  "        FROM Lap t1 "
                  "        WHERE run = 1 "
                  "            AND t1.pilot = Pilot.id "
                  "            AND time NOT NULL "
                  "        ORDER BY time ASC LIMIT 1"
                  "    )"
                  "    AS best_time "
                  "FROM "
                  "    Pilot "
                  "WHERE "
                  "    Pilot.id IN (SELECT Lap.pilot FROM Lap WHERE Lap.run = 1) "
                  "ORDER BY laps DESC";
    QSqlQuery classementQuery(str);
    m_model->setQuery(classementQuery);
    m_model->setHeaderData(0, Qt::Horizontal, tr("Prénom"));
    m_model->setHeaderData(1, Qt::Horizontal, tr("Nom"));
    m_model->setHeaderData(2, Qt::Horizontal, tr("Surnom"));
    m_model->setHeaderData(3, Qt::Horizontal, tr("Tours"));
    m_model->setHeaderData(4, Qt::Horizontal, tr("Meilleur temps au tour"));

    m_pilotModel = new QSqlRelationalTableModel(this, m_database);
    m_pilotModel->setTable("Pilot");
    if (!m_pilotModel->select()) {
        return;
    }

    // Init lapcounters extentions

    QDir plugDir = QDir(qApp->applicationDirPath());

    if(plugDir.cd("./plugins")) {

        foreach(QString file, plugDir.entryList(QDir::Files)) {
            QPluginLoader loader(plugDir.absoluteFilePath(file));

            if(QObject *plugin = loader.instance()) {

                m_lapCounterPlugin = qobject_cast<LapCounterPlugin *>(plugin);

            }
        }
    }

    connect(m_lapCounterPlugin, &LapCounterPlugin::lapCounterConnected, this, &FreeCountCore::onLapCounterConnected);

    m_lapCounterPlugin->load();
}

void FreeCountCore::onLapCounterConnected(LapCounter *lapCounter)
{
    m_lapCounter = lapCounter;
    connect(m_lapCounter, &LapCounter::heartBeat, this, &FreeCountCore::onHeartBeat);
    connect(m_lapCounter, &LapCounter::carDetected, this, &FreeCountCore::onCarDetected);
}

QSqlRelationalTableModel *FreeCountCore::pilotModel() const
{
    return m_pilotModel;
}

QSqlQueryModel *FreeCountCore::model() const
{
    return m_model;
}

void FreeCountCore::onHeartBeat(quint32 timestamp)
{
    qDebug() << timestamp;
}

void FreeCountCore::onCarDetected(quint32 timestamp, quint16 carId, quint8 hitNumber, quint8 signalStrength)
{
    qDebug() << "Lap detected at " << timestamp << " ! Pilot id :" << carId;
    QSqlQuery query(m_database);
    query.prepare("INSERT INTO Lap (timestamp, pilot, run) "
                  "VALUES (:timestamp, (SELECT pilot FROM Chip WHERE data = :carId), :run)");
    query.bindValue(":timestamp", timestamp);
    query.bindValue(":carId", carId);
    query.bindValue(":run", 1);
    query.exec();
    /*
    m_times.insertMulti(carId,timestamp);
    ui->tableWidget->insertRow(0);
    ui->tableWidget->setItem(0,0,new QTableWidgetItem(m_pilotes.value(carId,"Pilot Name")));
    ui->tableWidget->setItem(0,1,new QTableWidgetItem(QString::number(carId)));
    if(m_times.values(carId).size() > 1) {
        float time = (m_times.values(carId).at(0) - m_times.values(carId).at(1)) / 1000.0;
        ui->tableWidget->setItem(0,2,new QTableWidgetItem(QString::number(time) + " s"));
        QProcess *p = new QProcess();
        QStringList args(m_pilotes.value(carId,"Pilot") + ", " + QString::number(time, 'f', 1).replace(".", " virgule ") + " seconde");
        args.append("|");
        args.append("festival");
        args.append("--tts");
        qDebug() << args;
        p->startDetached("echo", args);
        delete p;
    }
    else {
        ui->tableWidget->setItem(0,2,new QTableWidgetItem("Start"));
    }*/

}
