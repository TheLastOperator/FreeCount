#ifndef LAPCOUNTERPLUGIN_H
#define LAPCOUNTERPLUGIN_H

#include "lapcounter.h"

class LapCounterPlugin : public QObject
{
    Q_OBJECT
public:
    virtual ~LapCounterPlugin() = 0;

    virtual bool load() = 0;

signals:
    void lapCounterConnected(LapCounter *lapCounter);
};

Q_DECLARE_INTERFACE(LapCounterPlugin, "fr.thelastoperator.FreeCount.FreeCountCore.LapCounterPlugin")

#endif // LAPCOUNTERPLUGIN_H
