#-------------------------------------------------
#
# Project created by QtCreator 2016-11-07T18:09:47
#
#-------------------------------------------------

QT       -= gui

QT       += sql

TARGET = FreeCountCore
TEMPLATE = lib

DEFINES += FREECOUNTCORE_LIBRARY

SOURCES += freecountcore.cpp

HEADERS += freecountcore.h\
    freecountcore_global.h \
    lapcounter.h \
    lapcounterplugin.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
