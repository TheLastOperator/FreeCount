#ifndef FREECOUNTCORE_H
#define FREECOUNTCORE_H

#include <QtSql>
#include "freecountcore_global.h"
#include "lapcounter.h"
#include "lapcounterplugin.h"

class FREECOUNTCORESHARED_EXPORT FreeCountCore : public QObject
{
    Q_OBJECT
public:
    FreeCountCore();

    QSqlQueryModel *model() const;

    QSqlRelationalTableModel *pilotModel() const;

public slots:
    void onHeartBeat(quint32 timestamp);
    void onCarDetected(quint32 timestamp, quint16 carId, quint8 hitNumber, quint8 signalStrength);
    void onLapCounterConnected(LapCounter *lapCounter);

private:
    QSqlDatabase m_database;

    QSqlQueryModel *m_model;
    QSqlRelationalTableModel *m_pilotModel;

    LapCounterPlugin *m_lapCounterPlugin;
    LapCounter *m_lapCounter;

};

#endif // FREECOUNTCORE_H
