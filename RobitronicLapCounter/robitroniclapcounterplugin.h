#ifndef ROBITRONICLAPCOUNTERPLUGIN_H
#define ROBITRONICLAPCOUNTERPLUGIN_H

#include "lapcounterplugin.h"

class RobitronicLapCounterPlugin : public LapCounterPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "fr.thelastoperator.FreeCount.FreeCountCore.LapCounterPlugin" FILE "RobitronicLapCounter.json")
    Q_INTERFACES(LapCounterPlugin)

public:
    virtual bool load();
};

#endif // ROBITRONICLAPCOUNTERPLUGIN_H
