#-------------------------------------------------
#
# Project created by QtCreator 2016-11-07T18:07:30
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RobitronicLapCounterPlugin
TEMPLATE = lib
CONFIG += plugin

#DESTDIR = $$[QT_INSTALL_PLUGINS]/generic

SOURCES += \
    robitroniclapcounterplugin.cpp \
    robitroniclapcounter.cpp

HEADERS += \
    robitroniclapcounterplugin.h \
    robitroniclapcounter.h

DISTFILES += \
    RobitronicLapCounterPlugin.json

#unix {
#    target.path = /usr/lib
#    INSTALLS += target
#}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../FreeCountCore/release/ -lFreeCountCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../FreeCountCore/debug/ -lFreeCountCore
else:unix: LIBS += -L$$OUT_PWD/../FreeCountCore/ -lFreeCountCore

INCLUDEPATH += $$PWD/../FreeCountCore
DEPENDPATH += $$PWD/../FreeCountCore
