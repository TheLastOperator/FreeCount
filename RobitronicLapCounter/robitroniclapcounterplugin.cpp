#include "robitroniclapcounterplugin.h"
#include "robitroniclapcounter.h"

#include <QSerialPort>
#include <QSerialPortInfo>

bool RobitronicLapCounterPlugin::load()
{
    QList<QSerialPortInfo> serialPorts = QSerialPortInfo::availablePorts();
    foreach (QSerialPortInfo serialPortInfo, serialPorts) {
        if(serialPortInfo.isValid()) {
            emit lapCounterConnected(new RobitronicLapCounter(serialPortInfo));
        }
    }

    return true;
}
