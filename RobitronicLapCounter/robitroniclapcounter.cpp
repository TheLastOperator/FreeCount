#include "robitroniclapcounter.h"
#include <QDebug>
#include <QSerialPortInfo>

RobitronicLapCounter::RobitronicLapCounter(QSerialPortInfo serialPortInfo)
{
    m_serialPort = new QSerialPort(serialPortInfo);
    qDebug() << "New serialport : ";
    qDebug() << serialPortInfo.portName();
    m_serialPort->setBaudRate(38400);
    connect(m_serialPort, &QSerialPort::readyRead, this, &RobitronicLapCounter::onSerialInput);
    if(m_serialPort->open(QIODevice::ReadWrite)) {
        char initDatagram[] = {0x03,0xB9,0x01};
        m_serialPort->write(initDatagram);
    }
    else {
        qDebug() << "Fail";
    }
}

void RobitronicLapCounter::startRace()
{
    return;
}

void RobitronicLapCounter::stopRace()
{
    return;
}

void RobitronicLapCounter::writeCarId(quint8)
{
    return;
}

bool RobitronicLapCounter::isCarIdWritable()
{
    return false;
}

void RobitronicLapCounter::onSerialInput()
{   
    m_packet.append(m_serialPort->readAll());

    if(m_packet.size() > 2) {
        if(m_packet.at(0) == 0x0D && m_packet.at(2) == (char)0x84) { // Car packet
            if(m_packet.size() > 0x0D) {
                QDataStream dataStream(m_packet);
                quint8 packetLength;
                dataStream >> packetLength;
                quint8 checksum;
                dataStream >> checksum;
                quint8 packetType;
                dataStream >> packetType;
                quint8 c0, c1;
                quint16 carId;
                dataStream >> c0 >> c1;
                carId = (c1 << 8) | c0;
                quint16 blank;
                dataStream >> blank;
                quint8 t0, t1, t2, t3;
                quint32 timestamp;
                dataStream >> t0 >> t1 >> t2 >> t3;
                timestamp = (t3 << 24) | (t2 << 16) | (t1 << 8) | t0;
                quint8 hitNumber;
                dataStream >> hitNumber;
                quint8 signalstrength;
                dataStream >> signalstrength;
                emit carDetected(timestamp, carId, hitNumber, signalstrength);
                qDebug() << m_packet.left(m_packet.at(0)).toHex();
                qDebug() << QString::number(timestamp,16);
                m_packet.remove(0, m_packet.at(0));
            }
        }
        else if(m_packet.at(0) == 0x0B && m_packet.at(2) == (char)0x83) { // Timestamp packet
            if(m_packet.size() > 0x0B) {
                QDataStream dataStream(m_packet);
                quint8 packetLength;
                dataStream >> packetLength;
                quint8 checksum;
                dataStream >> checksum;
                quint8 packetType;
                dataStream >> packetType;
                quint8 t0, t1, t2, t3;
                quint32 timestamp;
                dataStream >> t0 >> t1 >> t2 >> t3;
                timestamp = (t3 << 24) | (t2 << 16) | (t1 << 8) | t0;
                quint32 blank;
                dataStream >> blank;
                emit heartBeat(timestamp);
                qDebug() << m_packet.left(m_packet.at(0)).toHex();
                qDebug() << QString::number(timestamp,16);
                m_packet.remove(0, m_packet.at(0));
            }
        }
        else {
            m_packet.clear();
        }
    }
}

