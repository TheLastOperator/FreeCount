#ifndef ROBITRONICLAPCOUNTER_H
#define ROBITRONICLAPCOUNTER_H

#include "lapcounter.h"
#include <QSerialPort>
#include <QTimer>

class RobitronicLapCounter : public LapCounter
{
    Q_OBJECT
public:
    RobitronicLapCounter(QSerialPortInfo serialPortInfo);

    void startRace();
    void stopRace();
    void writeCarId(quint8);
    bool isCarIdWritable();

private slots:
    void onSerialInput();

private:
    QSerialPort *m_serialPort;
    QByteArray m_packet;
};

#endif // ROBITRONICLAPCOUNTER_H
