#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "freecountcore.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    FreeCountCore m_core;
};

#endif // MAINWINDOW_H
