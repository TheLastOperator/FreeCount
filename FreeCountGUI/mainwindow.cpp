#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "pilotsform.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableView->setModel(m_core.model());

    PilotsForm *form =new PilotsForm(m_core.pilotModel());
    form->show();

}

MainWindow::~MainWindow()
{
    delete ui;
}


