#ifndef PILOTSFORM_H
#define PILOTSFORM_H

#include <QDataWidgetMapper>
#include <QSqlRelationalTableModel>
#include <QWidget>

namespace Ui {
class PilotsForm;
}

class PilotsForm : public QWidget
{
    Q_OBJECT

public:
    explicit PilotsForm(QSqlRelationalTableModel *model, QWidget *parent = 0);
    ~PilotsForm();

private:
    Ui::PilotsForm *ui;
    QDataWidgetMapper *m_mapper;
};

#endif // PILOTSFORM_H
