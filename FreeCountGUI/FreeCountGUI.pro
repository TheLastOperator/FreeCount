#-------------------------------------------------
#
# Project created by QtCreator 2016-11-07T17:50:24
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FreeCountGUI
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    pilotsform.cpp

HEADERS  += mainwindow.h \
    pilotsform.h

FORMS    += mainwindow.ui \
    pilotsform.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../FreeCountCore/release/ -lFreeCountCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../FreeCountCore/debug/ -lFreeCountCore
else:unix: LIBS += -L$$OUT_PWD/../FreeCountCore/ -lFreeCountCore

INCLUDEPATH += $$PWD/../FreeCountCore
DEPENDPATH += $$PWD/../FreeCountCore
