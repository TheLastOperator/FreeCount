#include "pilotsform.h"
#include "ui_pilotsform.h"

PilotsForm::PilotsForm(QSqlRelationalTableModel *model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PilotsForm)
{
    ui->setupUi(this);

    ui->pilotsTableView->setModel(model);
    ui->pilotsTableView->setColumnHidden(model->fieldIndex("id"), true);
    ui->pilotsTableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->pilotsTableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    m_mapper = new QDataWidgetMapper(this);
    m_mapper->setModel(model);
    m_mapper->addMapping(ui->pilotIdEdit, 0);
    m_mapper->addMapping(ui->pilotFirstnameEdit, 1);
    m_mapper->addMapping(ui->pilotLastnameEdit, 2);
    m_mapper->addMapping(ui->pilotNicknameEdit, 3);
    m_mapper->addMapping(ui->pilotLevelSpinBox, 4);

    connect(ui->pilotsTableView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)), m_mapper, SLOT(setCurrentModelIndex(QModelIndex)));

    ui->pilotsTableView->setCurrentIndex(model->index(0, 0));

    m_mapper->toFirst();
}

PilotsForm::~PilotsForm()
{
    delete ui;
}
